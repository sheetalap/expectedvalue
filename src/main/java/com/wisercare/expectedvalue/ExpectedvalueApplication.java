package com.wisercare.expectedvalue;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ExpectedvalueApplication {

	public static void main(String[] args) {
		SpringApplication.run(ExpectedvalueApplication.class, args);
	}

}
