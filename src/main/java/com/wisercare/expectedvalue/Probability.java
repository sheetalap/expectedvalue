package com.wisercare.expectedvalue;

import lombok.Data;

@Data
//Expresses how likely a treatment (e.g. medicine) is to cause a particular outcome (e.g. side effect)
public class Probability
{
    private Outcome outcome;
    private Treatment treatment;
    private Double percentChance;

    public Probability(Treatment treatment, Outcome outcome, Double probability) {
        this.outcome = outcome;
        this.treatment = treatment;
        this.percentChance = probability;
    }
}
